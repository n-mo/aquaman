import csv
import time
import datetime

input_file = "donnees.txt"
output_file = "resultats.csv"
interval = 1  # Temps d'attente entre chaque lecture (en secondes)

while True:
    try:
        # Ouvre le fichier d'entrée en mode lecture
        with open(input_file, "r") as file:
            # Lit les données du fichier CSV
            lecture = csv.reader(file)

            # Détermine le nom du fichier de sortie en fonction de la date
            date = datetime.date.today()
            output_filename = f"{output_file}_{date}.csv"

            # Ouvre un nouveau fichier CSV en mode écriture
            with open(output_filename, "w", newline="") as outfile:
                # Initialise le writer pour écrire dans le fichier CSV
                writer = csv.writer(outfile, delimiter=";")

                # Ajoute une ligne d'en-tête au fichier CSV
                writer.writerow(["heure", "ph", "temperature"])

                # Parcourt chaque ligne du fichier d'entrée
                for row in lecture:
                    # Sépare les valeurs de chaque ligne en deux parties (pH et température)
                    res = row[0].split(";")

                    # Ajoute une ligne au fichier CSV avec les valeurs pH et température, ainsi que l'heure actuelle
                    now = datetime.datetime.now()
                    writer.writerow([now.strftime("%Y-%m-%d %H:%M:%S"), res[0], res[1]])

                    # Affiche les valeurs pH et température à la console, avec l'heure actuelle
                    print(now.strftime("%Y-%m-%d %H:%M:%S"), "pH:", res[0], "/", "temperature", res[1])

        # Attend un certain temps avant de recommencer la boucle
        time.sleep(interval)

    except IOError:
        # En cas d'erreur lors de la lecture du fichier, affiche un message d'erreur
        print("Problème de lecture")