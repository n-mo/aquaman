import csv
import matplotlib.pyplot as plt
import datetime
import time

while True:
    try:
        # Détermine le nom du fichier de sortie en fonction de la date
        date = datetime.date.today()
        output_filename = f"resultats.csv_{date}.csv"

        # Lit les données du fichier CSV
        with open(output_filename, "r") as file:
            reader = csv.reader(file, delimiter=";")
            next(reader)  # Ignore la première ligne d'en-tête

            # Initialise les listes pour stocker les valeurs du temps et du pH
            times = []
            ph_values = []

            # Parcourt chaque ligne du fichier d'entrée
            for row in reader:
                # Convertit la date et l'heure en objets datetime
                time_str = row[0].split(" ")[1]  # Sépare l'heure de la date
                time_obj = datetime.datetime.strptime(time_str, "%H:%M:%S").time()

                # Convertit l'objet datetime.time en chaîne de caractères représentant l'heure, les minutes et les secondes
                time_str = time_obj.strftime("%H:%M:%S")

                # Ajoute le temps et le pH aux listes
                times.append(time_str)
                ph_values.append(float(row[1]))

            # Trace le graphique
            plt.plot(times, ph_values)
            plt.xlabel("Heure:Minute:Seconde")
            plt.ylabel("pH")
            plt.title(f"Variation du pH le {date}")

            plt.xticks(rotation=45)  # Inclinaison des étiquettes de l'axe des abscisses pour une meilleure lisibilité
            plt.show()

        # Attend 1 secondes avant de recommencer la boucle
        time.sleep(1)

    except IOError:
        # En cas d'erreur lors de la lecture du fichier, affiche un message d'erreur
        print("Problème de lecture")
