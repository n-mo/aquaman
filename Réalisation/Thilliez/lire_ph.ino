
// config
const int PIN_PH = 13;
const int SENSOR_PIN = 0;


float lecture_pH(void)
{
  float compteur = 0;
  for (int i=1; i<=10; i++)       //Get 10 sample value from the sensor for smooth the value
  { 
    if ( (i!=1) && (i!=10) )
      compteur = compteur + analogRead(SENSOR_PIN);
    delay(10);
  }

  float avgValue = compteur / 8;          //donne la valeur analogique moyenne
  float phValue  = avgValue * 5.0 / 1024; //convertie la valure analog moy en millivolt en produit en croix

  return 3.5 * phValue;
}

void affichage_pH(float phValue)
{
 Serial.print("    pH:");  
 Serial.print(phValue,DEC);
 Serial.println(" ");
 digitalWrite(PIN_PH, HIGH);       
 delay(800);
 digitalWrite(PIN_PH, LOW); 
}

void setup()
{
 pinMode(PIN_PH, OUTPUT);  
 Serial.begin(9600);  
 Serial.println("Ready");    //Test the serial monitor
}

void loop()
{
  float pH = lecture_pH();
  affichage_pH(pH);
}
