import serial
import time

serial=serial.Serial('/dev/ttyACM0',9600) # on defini un objet serial avec l adresse du port

while True:
    serial.flushInput();serial.flushOutput() #on nettoie les buffer
    number=(serial.readline().decode("utf8", errors="replace")) # on lit sur le port serie et on l affecte a une variable
    print(number)          # on affiche la variable
    file = open("data.txt","a")
    file.write(number)
    file.close()
    